import requests, json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_pexels_image_url(city, state):
    pexels_url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    params = {
        "query": f"{city},{state} city, state",
        "per_page": 1,
    }
    try:
        response = requests.get(pexels_url, headers=headers, params=params)
        data = json.loads(response.content)


        if data.get("photos") and len(data["photos"]) > 0:
            picture_url = data["photos"][0]["src"]["original"]
            return picture_url
        else:

            return None

    except requests.exceptions.RequestException as error:
        print(f"Error making Pexels API request: {error}")
        return None


def get_weather_data(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"

    # Set up the request parameters
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }

    try:
        # OpenWeatherMap Geo API
        response = requests.get(geo_url, params=params)

        geo_data = json.loads(response.content)

        #lat and long from the geocoding response
        #if geo_data:
        location = geo_data[0].get("lat", None), geo_data[0].get("lon", None)
        # else:
        #      raise ValueError("No data in the geocoding response")

    except requests.exceptions.RequestException as error:
        # connection errors
        print(f"Error making geocoding API request: {error}")
        return {"weather": None}

    # Use lat and lon to fetch weather data
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "lat": location[0],
        "lon": location[1],
        "appid": OPEN_WEATHER_API_KEY,
    }

    try:
        # OpenWeatherMap API
        weather_response = requests.get(weather_url, params=weather_params)


        weather_data = json.loads(weather_response.content)

        # Extract relevant weather information
        temperature = weather_data.get("main", {}).get("temp")
        description = weather_data.get("weather", [{}])[0].get("description")

        # Return weather data in a dictionary
        return {
            "weather": {
                "temp": temperature,
                "description": description,
            }
        }

    except requests.exceptions.RequestException as e:
        # Handle request exceptions (e.g., connection errors)
        print(f"Error making OpenWeatherMap API request: {e}")
        return {"weather": None}
